variable1 = "Hello"
variable2 = "Hello"

if id(variable1) == id(variable2):
    print("The variables have the same ID")
else:
    print("The variables have different IDs")
